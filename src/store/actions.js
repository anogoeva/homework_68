import axios from 'axios';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER_REQUEST = 'SAVE_COUNTER_REQUEST';
export const SAVE_COUNTER_SUCCESS = 'SAVE_COUNTER_SUCCESS';
export const SAVE_COUNTER_FAILURE = 'SAVE_COUNTER_FAILURE';

export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const saveCounterRequest = () => ({type: SAVE_COUNTER_REQUEST});
export const saveCounterSuccess = () => ({type: SAVE_COUNTER_SUCCESS});
export const saveCounterFailure = () => ({type: SAVE_COUNTER_FAILURE});

const URL = 'https://general-bf34d-default-rtdb.firebaseio.com/counter.json';

export const fetchCounter = () => {
    return async (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        try {
            const response = await axios.get(URL);
            if (response.data === null) {
                dispatch(fetchCounterSuccess(0));
            } else {
                dispatch(fetchCounterSuccess(response.data));
            }
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    };
};

export const saveCounter = () => {
    return async (dispatch, getState) => {
        dispatch(saveCounterRequest());
        try {
            const response = await axios.put(URL, getState().counter);
            console.log(response);

            if (response.data) {
                dispatch(saveCounterSuccess());
            } else {
                dispatch(saveCounterSuccess());
            }

        } catch (e) {
            dispatch(saveCounterFailure());
        }
    };
};


